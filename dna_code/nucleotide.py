#coding:utf-8
from numpy.random import *
import pickle
import numpy as np
from datetime import datetime
from more_itertools import chunked
from .codon import Codon

# mRNA
class mRNA:
    def __init__(self):
        self.mRNA = ''
        self.DNA2RNA={'A':'U','T':'A','G':'C','C':'G'}
        self.cdn_ = None
        self.iconv_dic = None
        
    def __call__(self):
        return self.mRNA

    def set_codon(self,key):
        self.cdn_ = Codon(key)
        self.iconv_dic = self.cdn_.iconv_dic
    
    # DNA --> mRNA
    def translate(self,DNA):
        for dna in DNA:
            self.mRNA += self.DNA2RNA[dna]
            
    # アミノ酸からコドンへの変換(mRNAの生成)
    def amino2codon(self,amino_id):
        self.mRNA += self.cdn_[self.cdn_.choice_pattern(amino_id)]

    # コドンからアミノ酸への変換
    def codon2amino(self,codon):
        amino_id = self.iconv_dic[self.cdn_.cdn_.index(codon)]
        return amino_id, self.cdn_.cdn_amino[amino_id]
        
    # 開始コドンの追加
    def add_start_codon(self):
        self.mRNA += self.cdn_.start_cdn

    # 終止コドンの追加
    def add_end_codon(self):
        self.mRNA += self.cdn_[self.cdn_.choice_pattern(20)]

    # 開始コドンの前や終止コドンのあとに無意味なコドン列を追加して高度に暗号化
    # スプライシングの逆の工程
    def padding(self,length=20):
        for i in range(length):
            self.mRNA += self.cdn_[self.cdn_.choice_pattern(list(choice(list(range(20-1))+[20],1))[0])]
            

# DNA
class DNA:
    def __init__(self):
        self.DNA = ''
        self.RNA2DNA = {'U':'A','A':'T','C':'G','G':'C'}
        self.DNA_dic = {'A':'T','T':'A','G':'C','C':'G'}
        
    def __call__(self):
        return self.DNA
        
    # mRNA --> DNA
    def translate(self,mRNA):
        for RNA in mRNA:
            self.DNA += self.RNA2DNA[RNA]
