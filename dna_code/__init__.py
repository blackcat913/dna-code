from .codon import Codon,CodonParser
from .nucleotide import DNA, mRNA
from .generate import DNAcode

__version__ = "1.0.0"
