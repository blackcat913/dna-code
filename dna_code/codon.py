#coding:utf-8
# Codon
import numpy as np
from numpy.random import *

class Codon:
    def __init__(self,key):
        # コドンのパターンを生成
        self.cdn_ = self.make_cdn_list()
        self.key  = key
        # 開始コドン
        self.start_cdn='AUG'
        # 終止コドン
        self.end_cdn=['UAA','UAG','UGA']
        
        # コドンに対応するアミノ酸
        self.cdn_amino=['Phe','Leu','Ile','Val','Ser','Pro','Thr','Ala','Tyr','His',
                        'Gln','Asn','Lys','Asp','Glu','Cys','Trp','Arg','Gly','Met','END']
        
        # アミノ酸とコドンの対応関係
        self.cdn_num=[[0,1],[2,3,4,5,6,7],[8,9,10],[12,13,14,15],[16,17,18,19,56,57],[20,21,22,23],
                      [24,25,26,27],[28,29,30,31],[32,33],[36,37],[38,39],[40,41],[42,43],[44,45],
                      [46,47],[48,49],[51],[52,53,54,55,58,59],[60,61,62,63],[11],[34,35,50]]
        
        self.convert_dic = {}
        self.iconv_dic = {}
        self.set_new_dic()
        # codon番号からアミノ酸を特定するdictionary
        for key,value in self.convert_dic.items():
            for v in value:
                self.iconv_dic[v]=key
                
        self.cdn2amino = {self.cdn_[k]:self.cdn_amino[v] for k,v in self.iconv_dic.items()}
                
    def __getitem__(self,idx):
        return self.cdn_[idx]

    # mRNAのコドンリストの作成
    def make_cdn_list(self):
        cdn=[]
        for a in ['U','C','A','G']:
            for b in ['U','C','A','G']:
                for c in ['U','C','A','G']: 
                    cdn.append(b+a+c)
        return cdn
    
    # コドンとアミノ酸の再マッピング
    def set_new_dic(self):
        for i in range(len(self.key)):
            self.convert_dic[self.key[i]]=self.cdn_num[i]
        self.convert_dic[20]=self.cdn_num[20]
        
    # コドンとアミノ酸の関係をリセット
    def reset_dic(self):
        for i in range(20):
            self.convert_dic[i]=self.cdn_num[i]
        self.convert_dic[i] = self.cdn_num[20]
            
    # 複数のコドンから一つをランダムに選択
    def choice_pattern(self,number):
        return choice(self.convert_dic[number])


class CodonParser:
    def __init__(self,codon,silent_mode = False):
        self.codon = codon
        self.__act = False
        self.start_cdn = self.codon.start_cdn
        self.end_cdn = self.codon.end_cdn
        self.silent_mode = silent_mode
        self.parsed_list = []
        self.parsed_result = []

    def __call__(self):
        return ''.join(list(map(chr,self.parsed_result)))
        
    def parse(self,cdn):
        if cdn in self.start_cdn and self.__act is False or cdn in self.end_cdn and self.__act is True:
            self.__switch_act()
            return None
        if self.__act is False:
            return None
        if not(self.silent_mode):
            print(cdn)
        return cdn

    def append(self,x):
        return self.parsed_list.append(x)
    
    def __free(self):
        self.parsed_list = []
    
    def convert(self):
        self.parsed_result.append(self.icode_conv(self.parsed_list))
        self.__free()
    
    def __switch_act(self):
        self.__act = not(self.__act)
        if self.__act and not(self.silent_mode):
            print("### Parse Start ###")
        if self.__act is False:
            if not(self.silent_mode):
                print("### Parse Stop ###")
            self.convert()
            
    # 19進数(0~18)を10進数に変換
    def icode_conv(self,n_list,nd=19):
        return np.sum([nd**i*o[0] for i,o in enumerate(reversed(n_list))])
