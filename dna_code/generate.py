#coding:utf-8
from numpy.random import *
import pickle
import numpy as np
from datetime import datetime
from more_itertools import chunked
from .nucleotide import mRNA, DNA
from .codon import Codon, CodonParser
from tqdm import tqdm

# DNA化(暗号化)・アミノ酸列化(復号化)
class DNAcode:
    def __init__(self,key=[],padding=False,visual_mode=False,silent_mode=False):
        self.padding = padding
        self.visual_mode = visual_mode
        self.silent_mode = silent_mode
        
        if len(key)==0 or len(key)!=21:
            self.key_generator()
        else:
            self.key = key
        self.mRNA = mRNA()
        self.mRNA.set_codon(self.key)
        self.DNA = DNA()
        
    # mes -> DNA
    def mes2code(self,mes):
        """text -> mRNAオブジェクト,DNAオブジェクト,amino(text)"""
        self.DNA = DNA()
        self.mRNA.mRNA = ''
        try:
            self.code_conv(mes[0][0])
            f = lambda x:self.code_conv(x)
        except:
            f = lambda x:self.code_conv(ord(x))
        for m in tqdm(mes):
            # 前後にpadding
            self.mRNA.padding(randint(10)) if self.padding is True else ''
            self.mRNA.add_start_codon()
            for c in m:
                print(c,f(c)) if self.silent_mode is False else ''
                set(map(lambda x:self.mRNA.amino2codon(x),f(c)))
            self.mRNA.add_end_codon()
            self.mRNA.padding(randint(10)) if self.padding is True else ''
        self.DNA.translate(self.mRNA())
        amino = '-'.join([self.mRNA.cdn_.cdn2amino[''.join(o)] for o in chunked(self.mRNA(),3)])
        return amino,self.DNA,self.mRNA
                
    # DNA -> mes
    def code2mes(self,dna):
        """DNAオブジェクトもしくは,DNAのtext --> 元のtext """
        code = dna() if type(dna).__name__ == "DNA" else dna
        self.mRNA.mRNA = ""
        self.mRNA.translate(code)
        p = CodonParser(self.mRNA.cdn_,self.silent_mode)
        m = chunked(self.mRNA(),3)
        for _ in tqdm(range(int(len(self.mRNA())/3))):
            try:
                cdn = p.parse(''.join(next(m)))
                if cdn is not None:
                    p.append(self.mRNA.codon2amino(cdn))
            except StopIteration:
                break
        return p()

    # 鍵となるアミノ酸とコドンの関係を生成
    def key_generator(self,extract=False):
        data=[]
        while len(data) < 19:
            tmp=randint(19)
            if tmp not in data:
                data.append(tmp)
        # 開始コドン用
        data.append(19)
        # 終止コドン用
        data.append(20)
        if self.silent_mode is False:
            print('generate new key pattern:',data)
        if extract:
            with open('key_file_{}.dkf'.format(),'wb')as f:
                pickle.dump(data,f)
        # set key data
        self.key=data 
    
    # 10進数から19進数(0~18)に変換
    def code_conv(self,n,nd=19):
        result0=[]
        tmp1,tmp2 = n,1
        while tmp2 >= 1:
            tmp2 = int(tmp1/nd)
            result0.append(tmp1%nd)
            tmp1 = tmp2
        return list(reversed(result0))
